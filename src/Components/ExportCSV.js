import React from 'react'
import Button from '@material-ui/core/Button';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

export const ExportCSV = ({csvData, fileName,butonismi}) => {

    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const fileExtension = '.xlsx';

    const _header = ["FirmaAdi", "FirmaUnvani", "FirmaYetkilisiAdi", "FirmaYetkilisiSoyadi", "FirmaYetkilisiCepNo", "FirmaYetkilisiSabitNo", "FirmaAdres", "FirmaIl", "FirmaIlce", "YetkiliSMMMAdi", "YetkiliSMMMSoyadi", "YetkiliSMMMCepNo", "YetkiliSMMMSabitNo", "IslemYetkilisiAdi", "IslemYetkilisiSoyadi", "IslemYetkilisiCepNo", "IslemYetkilisiSabitNo", "SMMMAdres", "SMMMIl", "SMMMIlce", "VergiDariresi", "VergiNo", "SGKCari", "SGKGecmis", "Note", "UserId", "UpdateDate", "RandevuTarihi", "Status", "IslemYetkilisiMail", "YetkiliSMMMMail", "SirketSahipleriTel", "FirmaMail", "_id", "SistemeYuklendi", "Sifreler", "SistemKullaniciAdi", "KullaniciKodu", "SistemSfiresi1", "SistemSifresi2"];

    const exportToCSV = (csvData, fileName) => {
        const ws = XLSX.utils.json_to_sheet(csvData, {header: _header});
        const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };
        const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
        const data = new Blob([excelBuffer], {type: fileType});
        FileSaver.saveAs(data, fileName + fileExtension);
    }

    return (
        <Button  variant="contained" color="default"  onClick={(e) => exportToCSV(csvData,fileName)}>{butonismi}</Button>
    )
}