import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import { DateTimePicker } from "@material-ui/pickers";

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';

import InputMask from 'react-input-mask'

import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    DatePicker
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import trLocale from "date-fns/locale/tr";


import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { Grid } from '@material-ui/core';

import Chip from '@material-ui/core/Chip';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import MaterialTable from 'material-table';

function liste(SirketSahipleriTel) {
    var data = "";
    try {
        let str = SirketSahipleriTel.trim();
        data = str.split(",");
        data = data.reduce((acc, item) => {
            var str = item.split("-");
            var telefon = str[1].trim();
            telefon = telefon.replace(" ", "");
            telefon = telefon.replace("+9", "");
            if (telefon != "0" && telefon != "") {
                acc.push({ adi: str[0].trim(), telno: telefon })
            }
            return acc
        }, [])
    } catch (error) {
        data = []
    }

    return (
        <div id="history">
            <table >
                <tr  >
                    <th>Adi</th>
                    <th>Tel No</th>
                </tr>
                {data.map((d, i) => {
                    return (
                        <tr key={i}>
                            <td>{d.adi}</td>
                            <td>{d.telno}</td>
                        </tr>
                    )
                }
                )}
            </table>
        </div>
    );
}



class UpdateForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dialogOpen: false,
            dialogSifrelerOpen: false,
            PopupSifre: {   SistemSifresi2:"",
                            SistemSfiresi1:"",
                            KullaniciKodu:"",
                            SistemKullaniciAdi:""},
            Sifreler: props.firma.Sifreler ? props.firma.Sifreler : [],
            firmaMailDegistir: false,
            firmaYeniMail:'',
            IsEmailValid:false
        }
    }

    componentDidMount() {
        var IsEmailValid = this.checkEmail(this.props.firma.FirmaMail)
        this.setState({IsEmailValid:IsEmailValid})
}

    handleDialogClose = () => {
        this.setState({ dialogOpen: false });
    }

    handleDialogSend = () => {
        this.props._setState("RandevuTarihi", this.state.RandevuTarihi);
        this.props._sendDate(this.state.buttonId)
    }

    handleDialogOpen = (value) => {
        this.setState({ buttonId: value });
        this.setState({ dialogOpen: true });
    }

    handleDialogSifrelerClose = () => {
        this.setState({
            dialogSifrelerOpen: false
        }); 
        this.state.PopupSifre= {SistemSifresi2:"",
                                SistemSfiresi1:"",
                                KullaniciKodu:"",
                                SistemKullaniciAdi:""}
    }

    handleDialogSifrelerAdd = (value) => {

        var a = this.state.Sifreler.filter( (item) => {
            if (item.SistemSifresi2 == this.state.PopupSifre.SistemSifresi2 &&
                item.SistemSfiresi1 == this.state.PopupSifre.SistemSfiresi1 &&
                item.KullaniciKodu == this.state.PopupSifre.KullaniciKodu &&
                item.SistemKullaniciAdi == this.state.PopupSifre.SistemKullaniciAdi) {  
                return item
            }
        })
        if(a.length>0){

            this.handleDialogSifrelerClose();
            return;
        }

        var joined = this.state.Sifreler.concat(this.state.PopupSifre);
        this.setState({ Sifreler: joined })

        this.props._setState("Sifreler", joined);

        this.handleDialogSifrelerClose();
    }

    handleDialogSifrelerOpen = (value) => {
          
        this.setState({ dialogSifrelerOpen: true });
    }

    handlePasswordListDelete = (value) => {
        //this.state.Sifreler=this.state.Sifreler.push(value);
        //   this.setState();
    }

    _handleDateChange = (inputName) => value => {
        console.log(value);
        this.setState({
            [inputName]: value,
        })
    };

    _handleSifrelerChange = (inputName, value) => {
        this.setState({
            PopupSifre: {
                ...this.state.PopupSifre,
                [inputName]: value
            }
        })
    };

    _handleMailChanged = (value)  => {
        alert(this.state.firmaYeniMail)
        this.props._setState()
    }

    checkEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    deleteRow = (row) => {  
        var a = this.state.Sifreler.filter( (item) => {
            if (item.SistemSifresi2 == row.SistemSifresi2 &&
                item.SistemSfiresi1 == row.SistemSfiresi1 &&
                item.KullaniciKodu == row.KullaniciKodu &&
                item.SistemKullaniciAdi == row.SistemKullaniciAdi) {
                console.log("silinen şifre ");
                console.log(item)
            } else {
                return item
            }
        })
        this.setState({ Sifreler: a });  
    }

    render() {
        const { firma, handleChange, _sendDate, _setState } = this.props;

        let email;
        if (this.state.firmaMailDegistir) {
            email = <div>                                 
                        <TextField
                            autoFocus
                            variant="outlined"
                            fullWidth
                            type='text'
                            id="FirmaMail"
                            label="Firma Mail"
                            className=''
                            value={this.state.firmaYeniMail}
                            onChange={(e) => {
                                var IsEmailValid = this.checkEmail(e.target.value)
                                this.setState({firmaYeniMail:e.target.value, IsEmailValid:IsEmailValid})
                            }}
                            margin="normal"
                        />
                        <Button 
                            variant="outlined" 
                            color="primary"
                            disabled = {!this.state.IsEmailValid} 
                            onClick = {() => {
                                _setState('FirmaMail', this.state.firmaYeniMail)
                                this.setState({firmaMailDegistir:false})}
                            }>Kaydet</Button>
                        <Button variant="outlined" color="secondary" style = {{marginLeft:8}}
                            onClick = {() => {
                                this.setState({firmaMailDegistir:false})}
                            }>Iptal</Button>
                    </div>
        }
        else {
            email = <div>
                {firma.FirmaMail} 
                <Button variant="outlined" color="primary"
                    style={{marginLeft: 8}} 
                    onClick = {() => {
                        var firmaMail = firma.FirmaMail
                            this.setState({firmaMailDegistir:true, firmaYeniMail:firmaMail})}
                        }>Degistir</Button>
            </div>
        }

        return (
            <div style={{ minWidth: "100%", padding: 20, display: "inline-table" }} >
                <Card style={{ padding: 8, marginBottom: 18 }}>
                    <CardContent >
                        <div style={{ display: "flex", flexDirection: "row" }}>
                            <div style={{ flex: 1 }}>
                                <Typography color="textSecondary" gutterBottom>
                                    Firma Bilgileri
                        </Typography>
                                <h2>
                                    {firma.FirmaAdi}
                                </h2>
                                <Typography color="textSecondary">
                                    {firma.FirmaUnvani} 
                                    {firma.FirmaYetkilisiAdi}
                                    {firma.FirmaYetkilisiSoyadi}    
                                    {email}
                                    <br />   
                                    {firma.FirmaYetkilisiSabitNo}  
                                    {firma.FirmaYetkilisiCepNo}
                                </Typography>
                            </div>
                            <div style={{ flex: 1 }}>
                                {liste(firma.SirketSahipleriTel)}
                            </div>
                        </div>

                    </CardContent>
                </Card>

                <Card style={{ padding: 8, margin: 2 }}>
                    <table style={{ width: "100%", border: "none !important" }} >

                        <tr >
                            <td padding="none" component="td"  >
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="YetkiliSMMMAdi"
                                    label="Yetkili SMMM Adı"
                                    className=''
                                    value={firma.YetkiliSMMMAdi}
                                    onChange={handleChange('YetkiliSMMMAdi')}
                                    margin="normal"
                                />
                            </td>
                            <td padding="none" align="left" style={{ paddingLeft: 20 }}>
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="YetkiliSMMMSoyadi"
                                    label="Yetkili SMMM Soyadı"
                                    className=''
                                    value={firma.YetkiliSMMMSoyadi}
                                    onChange={handleChange('YetkiliSMMMSoyadi')}
                                    margin="normal"
                                />
                            </td>
                        </tr>

                        <tr >
                            <td padding="none" component="td"  >
                                <InputMask
                                    style={{ padding: 10, width: "100%" }}
                                    placeholder="Yetkili SMMM CepNo"
                                    mask="(0)999 999 99 99"
                                    maskChar=" "
                                    value={firma.YetkiliSMMMCepNo}
                                    onChange={handleChange('YetkiliSMMMCepNo')}
                                />
                            </td>
                            <td padding="none" align="left" style={{ paddingLeft: 20 }}>
                                <InputMask
                                    style={{ padding: 10, width: "100%" }}
                                    placeholder="Yetkili SMMM SabitNo"
                                    mask="(0)999 999 99 99"
                                    maskChar=" "
                                    value={firma.YetkiliSMMMSabitNo}
                                    onChange={handleChange('YetkiliSMMMSabitNo')}
                                />
                            </td>
                        </tr>

                        <tr >
                            <td padding="none" component="td"  >
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="YetkiliSMMMMail"
                                    label="Yetkili SMMM Mail"
                                    className=''
                                    value={firma.YetkiliSMMMMail}
                                    onChange={handleChange('YetkiliSMMMMail')}
                                    margin="normal"
                                />
                            </td>
                        </tr>

                        <tr >
                            <td colSpan={2} style={{ height: 2, background: "red", padding: 0 }}>

                            </td>
                        </tr>

                        <tr >
                            <td padding="none" component="td"  >
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="IslemYetkilisiAdi"
                                    label="İşlem Yetkilisi Adı"
                                    className=''
                                    value={firma.IslemYetkilisiAdi}
                                    onChange={handleChange('IslemYetkilisiAdi')}
                                    margin="normal"
                                />
                            </td>
                            <td padding="none" align="left" style={{ paddingLeft: 20 }}>
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="IslemYetkilisiSoyadi"
                                    label="İşlem Yetkilisi Soyadı"
                                    className=''
                                    value={firma.IslemYetkilisiSoyadi}
                                    onChange={handleChange('IslemYetkilisiSoyadi')}
                                    margin="normal"
                                />
                            </td>
                        </tr>

                        <tr >
                            <td padding="none" component="td"  >
                                <InputMask
                                    style={{ padding: 10, width: "100%" }}
                                    placeholder="Islem Yetkilisi CepNo"
                                    mask="(0)999 999 99 99"
                                    maskChar=" "
                                    value={firma.IslemYetkilisiCepNo}
                                    onChange={handleChange('IslemYetkilisiCepNo')}
                                />
                            </td>
                            <td padding="none" align="left" style={{ paddingLeft: 20 }}>
                                <InputMask
                                    style={{ padding: 10, width: "100%" }}
                                    placeholder="Islem Yetkilisi SabitNo"
                                    mask="(0)999 999 99 99"
                                    maskChar=" "
                                    value={firma.IslemYetkilisiSabitNo}
                                    onChange={handleChange('IslemYetkilisiSabitNo')}
                                />
                            </td>
                        </tr>

                        <tr >
                            <td padding="none" component="td"  >
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="IslemYetkilisiMail"
                                    label="İşlem Yetkilisi Mail"
                                    className=''
                                    value={firma.IslemYetkilisiMail}
                                    onChange={handleChange('IslemYetkilisiMail')}
                                    margin="normal"
                                />
                            </td>
                        </tr>

                        <tr >
                            <td colSpan={2} style={{ height: 2, background: "red", padding: 0 }}>

                            </td>
                        </tr>

                        <tr >
                            <td padding="none" component="td"  >
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="SMMMAdres"
                                    label="SMMM Adres"
                                    className=''
                                    value={firma.SMMMAdres}
                                    onChange={handleChange('SMMMAdres')}
                                    margin="normal"
                                />
                            </td>
                            <td padding="none" align="left" style={{ paddingLeft: 20 }}>
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="SMMMIl"
                                    label="SMMM İl"
                                    className=''
                                    value={firma.SMMMIl}
                                    onChange={handleChange('SMMMIl')}
                                    margin="normal"
                                />
                            </td>
                        </tr>

                        <tr >
                            <td padding="none" component="td"  >
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="SMMMIlce"
                                    label="SMMM İlçe"
                                    className=''
                                    value={firma.SMMMIlce}
                                    onChange={handleChange('SMMMIlce')}
                                    margin="normal"
                                />
                            </td>
                            <td padding="none" align="left" style={{ paddingLeft: 20 }}>

                            </td>
                        </tr>
                        <tr >
                            <td padding="none" component="td"  >
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="SistemKullaniciAdi"
                                    label="Sistem Kullanıcı Adı"
                                    className=''
                                    value={firma.SistemKullaniciAdi}
                                    onChange={handleChange('SistemKullaniciAdi')}
                                    margin="normal"
                                />
                            </td>
                            <td padding="none" align="left" style={{ paddingLeft: 20 }}>
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="KullaniciKodu"
                                    label="Kullanıcı Kodu"
                                    className=''
                                    value={firma.KullaniciKodu}
                                    onChange={handleChange('KullaniciKodu')}
                                    margin="normal"
                                />
                            </td>
                        </tr>

                        <tr >
                            <td padding="none" component="td"  >
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="SistemSfiresi1"
                                    label="Sistem Şifresi 1"
                                    className=''
                                    value={firma.SistemSfiresi1}
                                    onChange={handleChange('SistemSfiresi1')}
                                    margin="normal"
                                />
                            </td>
                            <td padding="none" align="left" style={{ paddingLeft: 20 }}>
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="SistemSifresi2"
                                    label="Sistem Şifresi 2"
                                    className=''
                                    value={firma.SistemSifresi2}
                                    onChange={handleChange('SistemSifresi2')}
                                    margin="normal"
                                />
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="2">

                                {/* <Paper  >
                                {this.state.Sifreler && this.state.Sifreler.map((data,index) => { 
                                    return (
                                    <Chip
                                        key={index} 
                                        label={data}
                                      
                                    />
                                    );
                                })}

                                <Fab color="primary" aria-label="add"  style={{ }} onClick={() => this.setState({dialogSifrelerOpen:true})}> 
                                    <AddIcon />
                                </Fab> 
                                </Paper> */}


                                <MaterialTable style={{ padding: 12 }}
                                    columns={[
                                        { title: 'SistemKullaniciAdi', field: 'SistemKullaniciAdi' },
                                        { title: 'KullaniciKodu', field: 'KullaniciKodu' },
                                        { title: 'SistemSfiresi1', field: 'SistemSfiresi1' },
                                        { title: 'SistemSifresi2', field: 'SistemSifresi2' },
                                    ]}
                                    options={{
                                        search: false,
                                        emptyRowsWhenPaging: false,
                                        paging: false
                                    }}
                                   
                                    data={this.state.Sifreler}
                                    title={<div>Sifreler
                                        <Fab color="primary" aria-label="add" size='small' style={{ marginLeft: 8 }} onClick={() => this.setState({ dialogSifrelerOpen: true })}>
                                            <AddIcon />
                                        </Fab>
                                    </div>}

                                    actions={[ 
                                            rowData => ({
                                                icon: 'delete',
                                                tooltip: 'sifre Sil',
                                                onClick: (event, rowData) => { this.deleteRow(rowData) }
                                            })
                                        ]}

                                />



                            </td>
                        </tr>

                        <tr >
                            <td padding="none" component="td"  >
                                <Checkbox
                                    checked={firma.SGKCari}
                                    onChange={handleChange('SGKCari')}
                                    value={firma.SGKCari ? false : true}
                                    inputProps={{
                                        'aria-label': 'primary checkbox',
                                    }}
                                />SGKCari
                            </td>
                            <td padding="none" align="left" style={{ paddingLeft: 20 }}>

                                <Checkbox
                                    checked={firma.SGKGecmis}
                                    onChange={handleChange('SGKGecmis')}
                                    value={firma.SGKGecmis ? false : true}
                                    inputProps={{
                                        'aria-label': 'primary checkbox',
                                    }}
                                />SGKGecmis


                        </td>
                        </tr>

                        <tr >

                            <td padding="none" colSpan={2} align="left" style={{ paddingLeft: 20 }}>
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    type='text'
                                    id="Note"
                                    label="Not"
                                    className=''
                                    value={firma.Note}
                                    onChange={handleChange('Note')}
                                    margin="normal"
                                />
                            </td>
                        </tr>

                        <tr>
                            <td padding="none" colSpan={2} component="td"  >
                                <Grid container spacing={3}>
                                    <Grid item xs={3}>
                                        <Button
                                            style={{ height: 80 }}
                                            fullWidth
                                            color="primary"
                                            variant="contained"
                                            onClick={() => _sendDate(100)}>
                                            BAŞARILI
                                    </Button>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <Button
                                            style={{ height: 80 }}
                                            fullWidth
                                            color="primary"
                                            variant="contained"
                                            onClick={() => this.handleDialogOpen(8)}> 
                                            ULAŞILAMADI
                                    </Button>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <Button
                                            style={{ height: 80 }}
                                            fullWidth
                                            color="primary"
                                            variant="contained"
                                            onClick={() => _sendDate(5)}>
                                            YANLIŞ NUMARA
                                    </Button>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <Button
                                            style={{ height: 80 }}
                                            fullWidth
                                            color="primary"
                                            variant="contained"
                                            onClick={() => _sendDate(7)}>
                                            FİRMA Kapanmış
                                    </Button>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <Button
                                            style={{ height: 80 }}
                                            fullWidth
                                            color="primary"
                                            variant="contained"
                                            onClick={() => this.handleDialogOpen(2)}> 
                                            Müsaİt Değil
                                    </Button>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <Button
                                            style={{ height: 80 }}
                                            fullWidth
                                            color="primary"
                                            variant="contained"
                                            onClick={() => this.handleDialogOpen(4)}> 
                                            Şİfre Bekleniyor
                                    </Button>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <Button
                                            style={{ height: 80 }}
                                            fullWidth
                                            color="primary"
                                            variant="contained"
                                            onClick={() => this.handleDialogOpen(3)}> 
                                            Tekrar Aranacak
                                    </Button>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <Button
                                            style={{ height: 80 }}
                                            fullWidth
                                            color="primary"
                                            variant="contained"
                                            onClick={() => _sendDate(6)}>
                                            DANIŞMANI VAR
                                    </Button>
                                    </Grid>
                                </Grid>
                            </td>
                        </tr>
                    </table>
                </Card>
                <Dialog
                    style={{ padding: 20 }}
                    open={this.state.dialogOpen}
                    keepMounted
                    onClose={this.handleDialogClose}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description" >
                    <DialogContent >
                        <MuiPickersUtilsProvider locale={trLocale} utils={DateFnsUtils} >
                            <DateTimePicker
                                value={this.state.RandevuTarihi}
                                disablePast
                                onChange={this._handleDateChange('RandevuTarihi')}
                                label="Randevu Saati"
                                showTodayButton
                                format="yyyy/MM/dd HH:mm"
                            />
                        </MuiPickersUtilsProvider>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleDialogClose} color="primary">
                            Iptal
                        </Button>
                        <Button onClick={this.handleDialogSend} color="primary">
                            Tamam
                        </Button>
                    </DialogActions>
                </Dialog>



                <Dialog
                    style={{ padding: 20 }}
                    open={this.state.dialogSifrelerOpen}
                    keepMounted
                    onClose={this.handleDialogSifrelerClose}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description" >
                    <DialogContent >
                        <table>
                            <tr >
                                <td padding="none" component="td"  >
                                    <TextField
                                        variant="outlined"
                                        fullWidth
                                        type='text'
                                        id="SistemKullaniciAdi"
                                        label="Sistem Kullanıcı Adı"
                                        className=''
                                        value={this.state.PopupSifre.SistemKullaniciAdi}
                                        onChange={(e) => this._handleSifrelerChange('SistemKullaniciAdi', e.target.value)}
                                        margin="normal"
                                    />
                                </td>
                                <td padding="none" align="left" style={{ paddingLeft: 20 }}>
                                    <TextField
                                        variant="outlined"
                                        fullWidth
                                        type='text'
                                        id="KullaniciKodu"
                                        label="Kullanıcı Kodu"
                                        className=''
                                        value={this.state.PopupSifre.KullaniciKodu}
                                        onChange={(e) => this._handleSifrelerChange('KullaniciKodu', e.target.value)}
                                        margin="normal"
                                    />
                                </td>
                            </tr>

                            <tr >
                                <td padding="none" component="td"  >
                                    <TextField
                                        variant="outlined"
                                        fullWidth
                                        type='text'
                                        id="SistemSfiresi1"
                                        label="Sistem Şifresi 1"
                                        className=''
                                        value={this.state.PopupSifre.SistemSfiresi1}
                                        onChange={(e) => this._handleSifrelerChange('SistemSfiresi1', e.target.value)}
                                        margin="normal"
                                    />
                                </td>
                                <td padding="none" align="left" style={{ paddingLeft: 20 }}>
                                    <TextField
                                        variant="outlined"
                                        fullWidth
                                        type='text'
                                        id="SistemSifresi2"
                                        label="Sistem Şifresi 2"
                                        className=''
                                        value={this.state.PopupSifre.SistemSifresi2}
                                        onChange={(e) => this._handleSifrelerChange('SistemSifresi2', e.target.value)}
                                        margin="normal"
                                    />
                                </td>
                            </tr>
                        </table>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleDialogSifrelerClose} color="primary">
                            Iptal
                        </Button>
                        <Button onClick={this.handleDialogSifrelerAdd} color="primary">
                            ekle
                        </Button>
                    </DialogActions>
                </Dialog>


            </div>
        );
    }
}

export default UpdateForm;