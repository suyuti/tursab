import React from 'react'
import { CSVLink } from 'react-csv'
import Button from '@material-ui/core/Button';

export const ExportReactCSV = ({csvData, fileName,butonismi}) => {
    return (
        <Button variant="contained" color="default">
            <CSVLink data={csvData} filename={fileName}>{butonismi}</CSVLink>
        </Button>
    )
}