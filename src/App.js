import React, { Component } from "react"; 
import "./App.css";
import CssBaseline from "@material-ui/core/CssBaseline";

// import { PermissionData } from './command';

import {
   BrowserRouter as Router,
   Switch,
   Route,
   Link
} from "react-router-dom";

import HomePage from "./Pages/Home";
import HistoryPage from "./Pages/HistoryPage";
import ListPage from "./Pages/ListPage";
// import DashboardPage from "./Pages/Dashboard/Dashboard";

 
import LoginPage from "./Pages/LoginPage";
import Header from "./Pages/Header";

import AdminPage from "./Pages/AdminPage";

 
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import SendIcon from '@material-ui/icons/Send';
import ListAltIcon from '@material-ui/icons/ListAlt';
import RestoreIcon from '@material-ui/icons/Restore';


function NoMatch() {
   let location = window.location.href;

   return (
      <div>
         <h3>
            No match for <code>{location}</code>
         </h3>
      </div>
   );
}


class App extends Component {
   constructor(props) {
      super(props);
      this.state = {
         currentUser: JSON.parse(localStorage.getItem("user")) 
      };    

 
   }

   componentDidMount() {



      // authenticationService.currentUser.subscribe(x =>
      //    this.setState({
      //       currentUser: x,
      //       isAdmin: x && x.role === Role.Admin
      //    })
      // );
   }

   logout = () => {
      this.setState({ currentUser: null })
      localStorage.removeItem("user");
   }

   render() {
      return (
         <div  >
            <CssBaseline />
            <div style={{ 
               height: "100vh", 
            }}>
               {this.state.currentUser ?
                  <Router>
                     <div style={{
                        display: "flex",
                        height: "100%"
                     }}>
                        <Paper style={{
                           padding: "10px",
                           width: "200px",
                           background: "#f0f0f0"
                        }}>
                           <div style={{
                              background: "rgb(221, 221, 221)",
                              textAlign: "center",
                              paddingTop: 5,
                              paddingBottom: 5,
                              borderRadius: 8,
                              textTransform: "uppercase",
                           }}>  {  this.state.currentUser.FirstName  +" " + this.state.currentUser.LastName }   </div>
                           <MenuList>
                              <Link to="/" style={style.link}>
                                 <MenuItem>
                                    <ListItemIcon>
                                       <ListAltIcon fontSize="small" />
                                    </ListItemIcon>
                                    <Typography variant="inherit">ÇAGRI</Typography>
                                 </MenuItem>
                              </Link>
                              <Link to="/list" style={style.link}>
                                 <MenuItem>
                                    <ListItemIcon>
                                       <RestoreIcon fontSize="small" />
                                    </ListItemIcon>
                                    <Typography variant="inherit">Liste</Typography>
                                 </MenuItem>
                              </Link>
                              <Link to="/history" style={style.link}>
                                 <MenuItem>
                                    <ListItemIcon>
                                       <RestoreIcon fontSize="small" />
                                    </ListItemIcon>
                                    <Typography variant="inherit">Geçmiş</Typography>
                                 </MenuItem>
                              </Link>

                              { this.state.currentUser.IsAdmin==true &&
                                <Link to="/admin" style={style.link}>
                                 <MenuItem>
                                    <ListItemIcon>
                                       <RestoreIcon fontSize="small" />
                                    </ListItemIcon>
                                    <Typography variant="inherit">Admin</Typography>
                                 </MenuItem>
                              </Link>  
                              }
                             


                              <Link to="/login" style={style.link} onClick={this.logout}>
                                 <MenuItem>
                                    <ListItemIcon>
                                       <SendIcon fontSize="small" />
                                    </ListItemIcon>
                                    <Typography variant="inherit">Çıkış</Typography>
                                 </MenuItem>
                              </Link>
                           </MenuList>
                        </Paper>

                        <div style={{ flex: 1, padding: "10px" }}>
                          {/* <Header/> */}
                           <Switch>
                              <Route exact path="/" component={HomePage} />
                              
                              <Route path="/history" component={HistoryPage} />
                              <Route path="/list" component={ListPage} />
                              { this.state.currentUser.IsAdmin==true &&
                              <Route path="/admin" component={AdminPage} />
                              }


                              
                              <Route path="/:fid" component={HomePage} />
                              <Route path="*">
                                 <NoMatch />
                                 {/* <LoginPage /> */}
                              </Route>


                           </Switch>
                        </div>
                     </div>
                  </Router>
                  : <LoginPage />}
            </div>
         </div>
      );
   }
}
const style = {
   link: {
      textDecoration: 'none',
      display: 'block'
   }
}

export default App;
