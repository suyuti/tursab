
import { http } from './axios' 

function login(login) {
    return http.post('/users/authenticate', login)
}
 

export const loginService = {
    login 
};

