
import { http } from './axios'

function getAll() {
    return http.get("/firma/list")
}

function getNext(id) {
    if(id){
        return http.get(`/firma/${id}`) 
    }else{
        return http.get(`/firma/next`)
    }  
}

function create(firma) {
    return http.post('/firma', firma)
}

function update(firma) {
    return http.put(`/firma/${firma._id}`, firma)
}

function remove(firma) {
    return http.delete( `/firma/${firma._id}`)
}

function search(query) { 
    return http.post('/firma/search', {query:query})
}
export const firmaService = {
    getAll,
    getNext,
    create,
    update,
    remove,
    search
};

