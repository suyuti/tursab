import { BehaviorSubject } from 'rxjs';
import { API_URL } from './config' 
const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser'))).asObservable();

export function checkPermission(Permission) {
    //izin kontrolü
    var userPermissinonList=JSON.parse( localStorage.getItem("currentUser")).Permissions
    if(Permission.length === 0) {
        return true;
	}
    for (let index = 0; index < userPermissinonList.length; index++) {
        for (let i = 0; i < Permission.length; i++) {
            //path kontrolü
            if (Permission[i].id === userPermissinonList[index].id ) {
                return true;
            }
        } 
    }
    return false;
}
export function currentUserPermission() {
    return currentUserSubject
}

function pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}
export function convertDateToTimestamp(value) {
    if (value === undefined) return new Date().getTime();


    let tempDate = value;
    var Timestamp = new Date(value).getTime();
    return Timestamp
}


export function convertTimestampToDate(value) {
    if (value === undefined) return "1970-01-01"
    value = parseInt(value);
    var d = new Date(value);
    var gun = d.getDate();
    var ay = d.getMonth();
    var yil = d.getFullYear();
    var returnDate = `${yil}-${pad(ay)}-${pad(gun)}`;
    return returnDate;
}

export function pathToRegexConverter(value) {
    // var str = "/proje/:id/ispaketi/:id/";  böyle gelmesi gerekiyor
    var str = value;
    var patt = /[:][a-z]*[/]/g;
    str = str.replace(patt, "[0-9a-z]*/");
    patt = /[:][a-z]*/g;
    str = str.replace(patt, "[0-9a-z]*");
    patt = /[/]/g;
    str = str.replace(patt, "\\/");
    return new RegExp( str, "g");  

}

export function pathToRegexCompare(path, regexPath) {
    /// path = "/proje/244/ispaketi/212";  örnek path
    /// regexPath = "/proje/:pid/ispaketi/:id";  örnek regexPath
    var result = path.match(pathToRegexConverter(regexPath))
    return result == path;
}

 



 export var PermissionData={
	postUsersAuthenticate:{"id":1,"Aciklama":"user bilgilerini Kontrol eder\r","Method":"post"},
	getUsers:{"id":2,"Aciklama":"user bilgilerini getirir\r","Method":"get"},
	getUsersId:{"id":3,"Aciklama":"user id si verilen user ı getirir\r","Method":"get"},
	putUsersId:{"id":4,"Aciklama":"user bilgilerini günceller\r","Method":"put"},
	postUsers:{"id":5,"Aciklama":"yeni bir user ekler\r","Method":"post"},
	deleteUsersId:{"id":6,"Aciklama":" user bilgisini siler\r","Method":"delete"},
	putUsersIdRole:{"id":7,"Aciklama":"user id si verilen kişinin rollerini günceller\r","Method":"put"},
	getMusteri:{"id":8,"Aciklama":" Musteri listesini getirir\r","Method":"get"},
	getMusteriId:{"id":9,"Aciklama":" Musteri id si verilen musteri bilgilerini getirir\r","Method":"get"},
	putMusteriId:{"id":10,"Aciklama":" Musteri id si verilen musterinin bilgilerini günceller\r","Method":"put"},
	postMusteri:{"id":11,"Aciklama":" yeni bir müşteri oluşturur\r","Method":"post"},
	deleteMusteriId:{"id":12,"Aciklama":" musteri id si verilen Musteriyi siler\r","Method":"delete"},
	getMusteriIdProje:{"id":13,"Aciklama":" musteriye ait projeleri getirir\r","Method":"get"},
	getMusteriIdPersonel:{"id":14,"Aciklama":" musteriye ait personelleri getirir\r","Method":"get"},
	postMusteriIdPersonelPid:{"id":15,"Aciklama":"musteriye ait personel bilgisini günceller.\r","Method":"post"},
	getMusteriIdEkip:{"id":16,"Aciklama":" musteriye ait ekip listesini getirir\r","Method":"get"},
	getMusteriIdDepartman:{"id":17,"Aciklama":" musteriye ait departman listesini getirir \r","Method":"get"},
	postMusteriIdProje:{"id":18,"Aciklama":" musteriye yeni bir proje ekler\r","Method":"post"},
	postMusteriIdDepartman:{"id":19,"Aciklama":" musteriye yeni bir departman ekler\r","Method":"post"},
	postMusteriIdEkip:{"id":20,"Aciklama":" musteriye yeni bir ekip ekler\r","Method":"post"},
	postMusteriIdPersonel:{"id":21,"Aciklama":" musteriye yeni bir personel ekler\r","Method":"post"},
	getMusteriIdAdamay:{"id":22,"Aciklama":" Musterinin adam aylarini getirir\r","Method":"get"},
	postMusteriIdAdamay:{"id":23,"Aciklama":" Musteriye adam ay hesaplar\r","Method":"post"},
	getProje:{"id":24,"Aciklama":"projeleri getiri\r","Method":"get"},
	getProjeId:{"id":25,"Aciklama":"proje id si verilen projenin bilgilerini getirir\r","Method":"get"},
	putProjeId:{"id":26,"Aciklama":" proje id si verilen projenin bilgilerini günceller\r","Method":"put"},
	deleteProjeId:{"id":27,"Aciklama":"proje id si verilen projeyi siler\r","Method":"delete"},
	getProjePidIspaketi:{"id":28,"Aciklama":" projenin iş paketlerini getirir\r","Method":"get"},
	getPersonel:{"id":29,"Aciklama":"Personel listesi doner\r","Method":"get"},
	getPersonelId:{"id":30,"Aciklama":"Belli bir personeli doner\r","Method":"get"},
	deletePersonelId:{"id":31,"Aciklama":"Belli bir personeli siler\r","Method":"delete"},
	postPersonelPidMusteriMid:{"id":32,"Aciklama":"Personeli musteride olusturur\r","Method":"post"},
	postPersonel:{"id":33,"Aciklama":"Yeni Bir Personel oluşturur\r","Method":"post"},
	putPersonelPidDepartmanDid:{"id":34,"Aciklama":"Personelin departmanini set eder\r","Method":"put"},
	putPersonelPidEkipEid:{"id":35,"Aciklama":"Personeli ekibe atar\r","Method":"put"},
	putPersonelPidMusteriMid:{"id":36,"Aciklama":"Personeli musteriye atar\r","Method":"put"},
	putPersonelPidProfilPrid:{"id":37,"Aciklama":"Personelin profilini set eder\r","Method":"put"},
	putPersonelId:{"id":38,"Aciklama":"Belli bir personeli günceller\r","Method":"put"},
	getEgitimDurumu:{"id":39,"Aciklama":"egitimdurumu listesi doner\r","Method":"get"},
	postEgitimDurumu:{"id":40,"Aciklama":"egitimdurumu oluşturur\r","Method":"post"},
	getEgitimDurumuId:{"id":41,"Aciklama":"idsi verilen egitimdurumu getirir\r","Method":"get"},
	putEgitimDurumuId:{"id":42,"Aciklama":"egitimdurumu gunceller\r","Method":"put"},
	deleteEgitimDurumuId:{"id":43,"Aciklama":"egitimdurumu siler (Status = 0)\r","Method":"delete"},
	getGorev:{"id":44,"Aciklama":" Gorev listesi doner\r","Method":"get"},
	postGorev:{"id":45,"Aciklama":" Gorev Oluşturur\r","Method":"post"},
	getGorevId:{"id":46,"Aciklama":" idsi verilen Gorev getirir\r","Method":"get"},
	putGorevId:{"id":47,"Aciklama":" Gorev gunceller\r","Method":"put"},
	deleteGorevId:{"id":48,"Aciklama":" Gorev siler (Status = 0)\r","Method":"delete"},
	getPersonelProfil:{"id":49,"Aciklama":" Profil listesi doner\r","Method":"get"},
	postPersonelProfil:{"id":50,"Aciklama":" Profil oluşturur\r","Method":"post"},
	getPersonelProfilId:{"id":51,"Aciklama":" idsi verilen Profil getirir\r","Method":"get"},
	putPersonelProfilId:{"id":52,"Aciklama":" Profil gunceller\r","Method":"put"},
	deletePersonelProfilId:{"id":53,"Aciklama":" Profil siler (Status = 0)\r","Method":"delete"},
	getProjeTurleri:{"id":54,"Aciklama":" ProjeTurleri listesi doner\r","Method":"get"},
	postProjeTurleri:{"id":55,"Aciklama":" ProjeTurleri oluşturur\r","Method":"post"},
	getProjeTurleriId:{"id":56,"Aciklama":" idsi verilen ProjeTurleri getirir\r","Method":"get"},
	putProjeTurleriId:{"id":57,"Aciklama":" ProjeTurleri gunceller\r","Method":"put"},
	deleteProjeTurleriId:{"id":58,"Aciklama":" ProjeTurleri siler (Status = 0)\r","Method":"delete"},
	getProjeAlanlari:{"id":59,"Aciklama":" ProjeAlanlari listesi doner\r","Method":"get"},
	postProjeAlanlari:{"id":60,"Aciklama":" ProjeAlanlari oluşturur\r","Method":"post"},
	getProjeAlanlariId:{"id":61,"Aciklama":" idsi verilen ProjeAlanlari getirir\r","Method":"get"},
	putProjeAlanlariId:{"id":62,"Aciklama":" ProjeAlanlari gunceller\r","Method":"put"},
	deleteProjeAlanlariId:{"id":63,"Aciklama":" ProjeAlanlari siler (Status = 0)\r","Method":"delete"},
	getDepartman:{"id":64,"Aciklama":"Departman listesi doner\r","Method":"get"},
	postDepartman:{"id":65,"Aciklama":" Departman oluşturur\r","Method":"post"},
	getDepartmanId:{"id":66,"Aciklama":" Belli bir Departmanyi doner\r","Method":"get"},
	putDepartmanId:{"id":67,"Aciklama":" Departman gunceller\r","Method":"put"},
	deleteDepartmanId:{"id":68,"Aciklama":" Departmanyi siler (Status = 0)\r","Method":"delete"},
	getEkip:{"id":69,"Aciklama":" Ekip listesi doner\r","Method":"get"},
	postEkip:{"id":70,"Aciklama":" Ekip oluşturur\r","Method":"post"},
	getEkipId:{"id":71,"Aciklama":" idsi verilen Ekip getirir\r","Method":"get"},
	putEkipId:{"id":72,"Aciklama":" Ekip gunceller\r","Method":"put"},
	deleteEkipId:{"id":73,"Aciklama":" Ekip siler (Status = 0)\r","Method":"delete"},
	getBolge:{"id":74,"Aciklama":" Bolge listesi doner\r","Method":"get"},
	postBolge:{"id":75,"Aciklama":" Bolge oluşturur\r","Method":"post"},
	getBolgeId:{"id":76,"Aciklama":" Belli bir Bolge doner\r","Method":"get"},
	putBolgeId:{"id":77,"Aciklama":" Bolge gunceller\r","Method":"put"},
	deleteBolgeId:{"id":78,"Aciklama":" Bolge siler (Status = 0)\r","Method":"delete"},
	getSektor:{"id":79,"Aciklama":" Sektor listesi doner\r","Method":"get"},
	postSektor:{"id":80,"Aciklama":" Sektor oluşturur\r","Method":"post"},
	getSektorId:{"id":81,"Aciklama":" Belli bir Sektor doner\r","Method":"get"},
	putSektorId:{"id":82,"Aciklama":" Sektor gunceller\r","Method":"put"},
	deleteSektorId:{"id":83,"Aciklama":" Sektor siler (Status = 0)\r","Method":"delete"},
	getIspaketleri:{"id":84,"Aciklama":" IsPaketleri listesi doner\r","Method":"get"},
	postIspaketleri:{"id":85,"Aciklama":" IsPaketleri oluşturur\r","Method":"post"},
	getIspaketleriId:{"id":86,"Aciklama":" idsi verilen IsPaketleri getirir\r","Method":"get"},
	putIspaketleriId:{"id":87,"Aciklama":" IsPaketleri gunceller\r","Method":"put"},
	deleteIspaketleriId:{"id":88,"Aciklama":" IsPaketleri siler (Status = 0)\r","Method":"delete"},
	postPermission:{"id":89,"Aciklama":" Permission oluşturur\r","Method":"post"},
};