import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

import { loginService } from "../Services";



function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="#">
                EXPERTO
        </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}








class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state={
            userName:"",
            userPassword:"",
            errorMessage:""
        }

    }

    componentWillMount() {

    }

    componentDidMount() {


    }

    _login = () => {
        this.state.errorMessage="";
        this.setState({});
        if(!this.state.userName || !this.state.userPassword) {
            this.state.errorMessage="Eksik Bilgi Girdiniz";
            this.setState({});
            return;
        }
        loginService.login(this.state).then(result => {
            if (result.status == 200) {   
                this.state.errorMessage="";
                localStorage.setItem("user", JSON.stringify(result.data) )
                window.location.href = "/"
            }else{ 
                this.state.errorMessage=result.messages;
                this.setState({});
            }  
        });  
    }

    _handleChange = (input) => e => {
        console.log(e.target.value, input);
        let inputValue = e.target.value;
        this.setState(() => ({
                ...this.state.EgitimDurumu,
                [input]: inputValue
        }));
    };

    render() {



        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div style={classes.paper}>
                    <Avatar style={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
         </Typography>
                    <form style={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            onChange={this._handleChange('userName')}
                            value={this.state.userName}
                        />
 

                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={this._handleChange('userPassword')}
                            value={this.state.userPassword}
                        />
                        {/* <FormControlLabel
                            control={<Checkbox value="remember" color="primary" />}
                            label="Remember me"
                        /> */}
                        {this.state.errorMessage &&
                            <div style={{padding:8,background:"#ff0000ba",color:"white",textAlign:"center",borderRadius:8,margin:8}}>
                            {this.state.errorMessage}
                            </div>
                        }
                        <Button
                            onClick={this._login}
                            fullWidth
                            variant="contained"
                            color="primary"
                            style={classes.submit}
                        >
                            Sign In
           </Button>

                    </form>
                </div>
                <Box mt={8}>

                   


                    <Copyright />
                </Box>
            </Container>
        );
    }
}








const classes = {
    '@global': {
        body: {
            backgroundColor: "white",
        },
    },
    paper: {
        marginTop: 50,
        display: 'flex',   
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: 1,
        backgroundColor: "red",
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: 1,
    },
    submit: {
        margin: 2,
    },
};



export default LoginPage;

