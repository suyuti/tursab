import React, { Component } from 'react';
import MaterialTable from 'material-table';
import { firmaService } from "../Services";
import CircularProgress from '@material-ui/core/CircularProgress';
import Checkbox from '@material-ui/core/Checkbox';



function liste(SirketSahipleriTel) { 
    var data="";
    try {
        let str = SirketSahipleriTel.trim();
         data = str.split(",");
        data = data.reduce((acc, item) => {
            var str = item.split("-");
            var telefon = str[1].trim();
            telefon = telefon.replace(" ", "");
            telefon = telefon.replace("+9", "");
            if (telefon != "0" && telefon != "" ) {
                acc.push({ adi: str[0].trim(), telno: telefon })
            }
            return acc
        }, []) 
    } catch (error) {
        data=[]
    } 

    return (
        <div id="history">
            <table >
                <tr  >
                    <th>Adi</th>
                    <th>Tel No</th>
                </tr> 
                {data.map((d, i) => {
                    return (
                        <tr key={i}>
                            <td>{d.adi}</td>
                            <td>{d.telno}</td>
                        </tr>
                    )
                }
                )}
            </table>
        </div>
    );
}


class HistoryPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            errorMessage: "",
            loaded:false
        }
    }
 

    componentDidMount() {

        firmaService.getAll().then(result => {
            if (result.status == 200) {
                this.state.errorMessage = "";
                
                var currentUserId=localStorage.getItem("user");
                console.log(currentUserId); 
                var filterData=result.data.filter(function (itemm) {
                    return itemm.UserId===currentUserId._id; 
                }) 
                

                filterData.sort(function(a,b){
                    return new Date(b.UpdateDate) - new Date(a.UpdateDate);
                  });
                console.log(filterData )
                this.setState({ data: filterData.sort() }) 
                this.setState({ loaded: true}) 
 
            } else {
                this.state.errorMessage = result.messages;
                this.setState({});
            }
        });

    }
    updateRow = (Row) => {
        window.location.href = "/" + Row._id;
    }


    render() {
        if( !this.state.loaded){
        return (
            <div style={{textAlign:"center"}}> 
              <CircularProgress   color="secondary" />
            </div>
          )
        }

        return (
            <div >
                {this.state.errorMessage &&
                    <div style={{ padding: 8, background: "#ff0000ba", color: "white", textAlign: "center", borderRadius: 8, margin: 8 }}>
                        {this.state.errorMessage}
                    </div>
                }
                <MaterialTable style={{ padding: 12 }}
                    columns={[
                        { title: 'Firma Belge No', field: 'FirmaBelgeNo' },
                        { title: 'Firma Adi', field: 'FirmaAdi' },
                        { title: 'Firma Unvani', field: 'FirmaUnvani' },
                        { title: 'Firma Yetkilisi Adi', field: 'FirmaYetkilisiAdi' },
                        { title: 'Status', field: 'Status' },
                        { title: 'Sisteme Yüklendi', field: 'SistemeYuklendi', render: rowData =>
                            <Checkbox
                                checked={rowData.SistemeYuklendi}
                            />
                        }
                    ]}
                    data={this.state.data}
                    detailPanel={[
                        {
                            tooltip: 'Show info',
                            render: rowData => {
                                return (
                                    <div
                                        style={{
                                            padding: 25,
                                            textAlign: 'left',
                                            color: 'black',
                                            backgroundColor: '#f0f0f0',
                                        }}
                                    >
                                        {liste(rowData.SirketSahipleriTel)}
                                    </div>
                                )
                            },
                        }]}
                    title="Çagrı Listesi"
                    options={{
                        search: true,
                        actionsColumnIndex: -1,
                        pageSize: 10,
                        emptyRowsWhenPaging: false,
                        pageSizeOptions:[10, 30, 50, 100]
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Çagrı Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        })
                    ]}
                />
            </div>
        );
    }
}

export default HistoryPage;



// class MultipleDetailPanels extends React.Component {
//     render() {
//         return (
//             <MaterialTable
//                 title="Multiple Detail Panels Preview"
//                 columns={[
//                     { title: 'Name', field: 'name' },
//                     { title: 'Surname', field: 'surname' },
//                     { title: 'Birth Year', field: 'birthYear', type: 'numeric' },
//                     {
//                         title: 'Birth Place',
//                         field: 'birthCity',
//                         lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' },
//                     },
//                 ]}
//                 data={[
//                     { name: 'Mehmet', surname: 'Baran', birthYear: 1987, birthCity: 63 },
//                     { name: 'Zerya Betül', surname: 'Baran', birthYear: 2017, birthCity: 34 },
//                 ]}
//                 detailPanel={[
//                     {
//                         tooltip: 'Show Name',
//                         render: rowData => {
//                             return (
//                                 <div
//                                     style={{
//                                         fontSize: 100,
//                                         textAlign: 'center',
//                                         color: 'white',
//                                         backgroundColor: '#43A047',
//                                     }}
//                                 >
//                                     {rowData.name}
//                                 </div>
//                             )
//                         },
//                     },
//                     {
//                         icon: 'account_circle',
//                         tooltip: 'Show Surname',
//                         render: rowData => {
//                             return (
//                                 <div
//                                     style={{
//                                         fontSize: 100,
//                                         textAlign: 'center',
//                                         color: 'white',
//                                         backgroundColor: '#E53935',
//                                     }}
//                                 >
//                                     {rowData.surname}
//                                 </div>
//                             )
//                         },
//                     },
//                     {
//                         icon: 'favorite_border',
//                         openIcon: 'favorite',
//                         tooltip: 'Show Both',
//                         render: rowData => {
//                             return (
//                                 <div
//                                     style={{
//                                         fontSize: 100,
//                                         textAlign: 'center',
//                                         color: 'white',
//                                         backgroundColor: '#FDD835',
//                                     }}
//                                 >
//                                     {rowData.name} {rowData.surname}
//                                 </div>
//                             )
//                         },
//                     },
//                 ]}
//             />
//         )
//     }
// }
