import React, { Component } from 'react'; 
import { firmaService } from "../Services";
import  {ExportReactCSV} from '../Components/ExportReactCSV'
import {ExportCSV } from '../Components/ExportCSV'
import CircularProgress from '@material-ui/core/CircularProgress';

class AdminPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            errorMessage: "",
            loaded:false
        }

    }

    componentWillMount() {

    }

    componentDidMount() {
        firmaService.getAll().then(result => {
            if (result.status == 200) {
                this.state.errorMessage = "";
                this.state.loaded = true;

                var data =     result.data.map(function (item) { 
                        if(item.Sifreler.length > 0 ){

                            for (let index = 1; index < item.Sifreler.length+1 ; index++) {

                                item[`SistemKullaniciAdi.${index-1}`] = item.Sifreler[index-1].SistemKullaniciAdi ? item.Sifreler[index-1].SistemKullaniciAdi  : ""  ;
                                
                                item[`KullaniciKodu.${index-1}`] = item.Sifreler[index-1].KullaniciKodu ? item.Sifreler[index-1].KullaniciKodu  : ""  ;

                                item[`SistemSfiresi1.${index-1}`] = item.Sifreler[index-1].SistemSfiresi1 ? item.Sifreler[index-1].SistemSfiresi1  : ""  ;

                                item[`SistemSifresi2.${index-1}`] = item.Sifreler[index-1].SistemSifresi2 ? item.Sifreler[index-1].SistemSifresi2  : ""  ;
                            }
                            
                        }

                        return  item

                      }) 

console.log(data )
                this.setState({ data: data })
            } else {
                this.state.errorMessage = result.messages;
                this.setState({});
            }
        });

    }

 

    render() {
        if (!this.state.loaded){
            return ( <div style={{textAlign:"center"}}> <CircularProgress   color="secondary" />  </div>  )
        }
        return (
            <div>

            {/* <ExportReactCSV csvData={this.state.data} fileName="aaaaa.csv" butonismi="indir" /> */}

            <ExportCSV csvData={this.state.data} fileName="tursab" butonismi="indir" />
            

            {this.state.errorMessage &&
                            <div style={{    position: "absolute", bottom: 0,display:"block",padding:8,background:"#ff0000ba",color:"white",textAlign:"center",borderRadius:8,margin:8, height: "40px"}}>
                            {this.state.errorMessage}
                            </div>
                        }

            </div>
        );
    }
}
 

export default AdminPage;