import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
 import UpdateForm from "../Components/UpdateForm"
 import { firmaService } from "../Services";
 import CircularProgress from '@material-ui/core/CircularProgress';
    
class Home extends Component {
    constructor(props) {
        super(props);
        this.state={ 
            data:"",
            loaded:true,
            showForm:false
        }

        this.firmaId=this.props.match.params.fid
        if(this.firmaId){
            this._getData(this.firmaId)
        }
    }

    componentWillMount() {

    }

    componentDidMount() {

    }
    _setState=(state,value)=>{
        this.setState(() => ({
            data: {
                ...this.state.data,
                [state]: value
            }
        }));
        this.state.data[state]= value
    }

    handleChange = (input) => e => { 
        if (!e.target) {
            this.setState(() => ({
                data: {
                    ...this.state.data,
                    [input]: e
                }
            }));
        } 
        else {
            let inputValue = e.target.value;
            this.setState(() => ({
                data: {
                    ...this.state.data,
                    [input]: inputValue
                }
            }));
        }

        console.log(this.state.data)
    };

    _sendDate=(value)=>{
    // alert(  "dfgdf"+ value)

    // this.setState(() => ({
    //         data: {
    //             ...this.state.data,
                
    //         }
    //     })); 

        this.state.data.Status= value;
    console.log(this.state.data);
        firmaService.update(this.state.data).then(result => {
                if (result.status == 200) {   
                    this.state.errorMessage="";
                   
                    this.setState({showForm:false});  
                    this.setState({loaded:true});  
                    

                }else{ 
                    this.state.errorMessage=result.messages;
                    this.setState({});
                }  
            });   
    }

    _getData=(id)=>{ 
         this.setState({loaded:false});  
        firmaService.getNext(id).then(result => {
            if (result.status == 200) {   
                this.state.errorMessage="";
                this.setState({data:result.data[0]}); 
                this.setState({showForm:true});  
            }else{ 
                this.state.errorMessage=result.messages;
                this.setState({});
            }  
        });  

    }




    render() { 
        let button =<div style={{ display: "flex",flex:1,alignItems: "center", justifyContent: "center",}}><Button
                    style={{  width:200 ,height:200}}
                    color="primary"
                    variant="contained"
                    onClick={()=> this._getData()}>
                      {this.state.loaded ?   "YENİ GETİR":<CircularProgress   color="secondary" />  } 
                </Button> </div> 
        return (
            <div style={{ display: "flex", height: "100%", justifyContent: "center",    overflow: "auto",  }}>
                         {this.state.errorMessage &&
                            <div style={{    position: "absolute", bottom: 0,display:"block",padding:8,background:"#ff0000ba",color:"white",textAlign:"center",borderRadius:8,margin:8, height: "40px"}}>
                            {this.state.errorMessage}
                            </div>
                        }

             
                { !this.state.showForm ? button: <UpdateForm firma={this.state.data} handleChange={this.handleChange }  _sendDate={this._sendDate}  _setState={this._setState}/>     }
            </div>
        );
    }
}



export default Home;