import React, { Component } from 'react';
import { withStyles } from '@material-ui/styles';
import { fade, makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import TextField from '@material-ui/core/TextField';
import { firmaService } from "../Services";

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: "",
            errorMessage:""
        }
    }

    handleChange = (input) => e => {
        let inputValue = e.target.value;
        this.setState({
            [input]: inputValue
        });
    };

    _getData = () => {
        // window.location.href = "/2" 
     

        firmaService.search(this.state.search).then(result => {
            if (result.status == 200) {
              if(result.data._id ){
                 window.location.href = result.data._id  
              }else{
                this.state.errorMessage ="gerekli id bulunamamıştır";
                this.setState({});
              } 
            } else {
                this.state.errorMessage = result.messages;
                this.setState({});
            }
        }); 

    }

    render() {
        return (
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <div style={{ display: "flex" }} >
                            <div className=" ">
                                <SearchIcon />
                            </div>
                            <InputBase
                                style={{ color: "white", textAlign: "center" }}
                                placeholder="Search…"
                                value={this.state.search}
                                onChange={this.handleChange('search')}
                                inputProps={{ 'aria-label': 'search' }}
                            />

                            <Button variant="contained" onClick={() => this._getData()}>
                                ARA
                            </Button>
                        </div>
                      
                    </Toolbar>
                </AppBar>
                {this.state.errorMessage &&
                            <div style={{    position: "absolute", bottom: 0,right:0,display:"block",padding:8,margin:8,background:"#ff0000ba",color:"white",textAlign:"center",borderRadius:8, height: "40px"}}>
                            {this.state.errorMessage}
                            </div>
                        }
            </div>
        );
    }
}


export default Header;